# KCI Cognito Identity Sample #

このサンプルアプリケーションを動作させるためには以下の設定が必要です。

## Google+ APIの設定
1. Google+ APIを有効にする https://console.developers.google.com  
2. AndroidでGoogle+ APIを使用するために必要な設定ファイル（google-services.json）を生成してappディレクトリ直下に配置する https://developers.google.com/mobile/add  
3. Googleから払いだされたクライアントIDをstrings.xmlのgoogle_auth_client_idに設定する。
   
## Login with Amazonの設定
 * リンク先のセットアップを実施する http://login.amazon.com/android 

## APK署名の設定
 * keystore, build.gradleのsigningConfigsを適宜設定する。
  
## AWS Cognito Identityの設定
1. cognitoページ（https://ap-northeast-1.console.aws.amazon.com/cognito/home?region=ap-northeast-1）からcreate new identity poolする
2. Identity pool nameに適当なものを入力
3. Unauthenticated identitiesのEnable access to unauthenticated identitiesにチェック
4. Authentication providersに外部プロバイダから払いだされた情報を入力
5. Create Poolを押す
6. 右上のEdit identity poolボタンを押して表示されるIdentity pool IDを strings.xmlのamazon_cognito_identity_pool_idに設定する。
  
## AWS S3バケットの設定
* strings.xmlのbucket_nameにs3のバケット名を設定する。（サンプルアプリケーションに利用してよいバケットを持っていない場合は作成する）
