package jp.kurusugawa.sample.cognito.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import de.greenrobot.event.EventBus;
import jp.kurusugawa.sample.cognito.CognitoSampleApplication;
import jp.kurusugawa.sample.cognito.MainActivity;
import jp.kurusugawa.sample.cognito.R;
import jp.kurusugawa.sample.cognito.auth.ApplicationAccountManager;
import jp.kurusugawa.sample.cognito.auth.login.AmazonLogin;
import jp.kurusugawa.sample.cognito.auth.login.GoogleLogin;
import jp.kurusugawa.sample.cognito.service.S3DeleteObjectService;
import jp.kurusugawa.sample.cognito.service.S3ListObjectsService;
import jp.kurusugawa.sample.cognito.service.S3PutObjectService;
import jp.kurusugawa.sample.cognito.dialog.MessageDialog;

public class MainFragment extends Fragment {

    private CognitoSampleApplication mApplication;
    private View mView;

    private static final String ARG_SELECT_FILE_NAME = "ARG_SELECT_FILE_NAME";

    private static final int REQ_ERROR_DIALOG = 0x02;
    private static final int REQ_BEGIN_LOGGING = 0x04;
    private static final int REQ_CONFIRM_DELETE_DIALOG = 0x08;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_main, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mApplication = (CognitoSampleApplication)getActivity().getApplication();
        generateIdentityRequestIfNeeded();

        initializeIdLabel();
        initializeListView();
        initializeFeatureButton();
        initializeLoginButtons();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQ_BEGIN_LOGGING:
                if(resultCode != SensorLoggingFragment.RESPONSE_CODE_FINISH)
                    break;
                requestRefreshItems();
                break;

            case REQ_CONFIRM_DELETE_DIALOG:
                if (resultCode != MessageDialog.Button.POSITIVE)
                    break;
                final File tTargetFile = new File(mApplication.getRootDir(), data.getExtras().getString(ARG_SELECT_FILE_NAME));
                S3DeleteObjectService.startService(getActivity().getApplication(), tTargetFile);
                if (tTargetFile.exists())
                    tTargetFile.delete();
                requestRefreshItems();
                break;
        }
    }

    private void initializeIdLabel() {
        ((TextView)mView.findViewById(R.id.txtId)).setText(mApplication.getApplicationAccountManager().getCachedId());
    }

    private void initializeListView() {
        ((ListView)mView.findViewById(R.id.listFiles)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String tFileName = (String) parent.getAdapter().getItem(position);
                final Bundle tBundle = new Bundle();
                tBundle.putString(ARG_SELECT_FILE_NAME, tFileName);
                MessageDialog.newInstance("確認", String.format("%sを削除します。宜しいですか？", tFileName), MessageDialog.Button.buildYesNo(), REQ_CONFIRM_DELETE_DIALOG, tBundle, true, MainFragment.this).show(getActivity().getSupportFragmentManager());
            }
        });
        ((ListView)mView.findViewById(R.id.listFiles)).setEmptyView(mView.findViewById(R.id.emptyList));
    }

    private void initializeFeatureButton() {
        mView.findViewById(R.id.btnBeginSensorLog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.root, SensorLoggingFragment.newInstance(REQ_BEGIN_LOGGING, MainFragment.this)).commit();
            }
        });
    }

    private void initializeLoginButtons() {
        mView.findViewById(R.id.btnIssueGuestId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginAsGuest();
            }
        });

        mView.findViewById(R.id.btnLoginWithGoogle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWithGoogle();
            }
        });

        mView.findViewById(R.id.btnLoginWithAmazon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWithAmazon();
            }
        });
    }

    private void loginAsGuest() {
        mApplication.execute(new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                EventBus.getDefault().post(new MainActivity.UpdateProgressRequestEvent().withVisible(true));
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    return mApplication.getApplicationAccountManager().getIdentityId();
                } catch (Exception aCause) {
                    Log.e(MainFragment.class.getSimpleName(), "", aCause);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String aIdentityId) {
                EventBus.getDefault().post(new MainActivity.UpdateProgressRequestEvent().withVisible(false));
                if (aIdentityId == null) {
                    MessageDialog.newInstance("ゲストIDを発行出来ません", "インターネットに接続していることを確認して下さい", MessageDialog.Button.buildClose(), REQ_ERROR_DIALOG, true, MainFragment.this).show(getActivity().getSupportFragmentManager());
                }
            }
        });
    }

    private void loginWithGoogle() {
        mApplication.execute(new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                EventBus.getDefault().post(new MainActivity.UpdateProgressRequestEvent().withVisible(true));
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    String tToken = new GoogleLogin(getActivity()).getToken();
                    return mApplication.getApplicationAccountManager().getIdentityId("accounts.google.com", tToken);
                } catch (Exception aCause) {
                    Log.e(MainFragment.class.getSimpleName(), "", aCause);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String aResult) {
                EventBus.getDefault().post(new MainActivity.UpdateProgressRequestEvent().withVisible(false));
                if (aResult == null) {
                    MessageDialog.newInstance("IDを取得できません", "再度お試し下さい", MessageDialog.Button.buildClose(), REQ_ERROR_DIALOG, true, MainFragment.this).show(getActivity().getSupportFragmentManager());
                }
            }
        });
    }

    private void loginWithAmazon() {
        mApplication.execute(new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                EventBus.getDefault().post(new MainActivity.UpdateProgressRequestEvent().withVisible(true));
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    AmazonLogin tLogin = new AmazonLogin(getActivity());
                    tLogin.login();
                    String tToken = tLogin.getToken();
                    return mApplication.getApplicationAccountManager().getIdentityId("www.amazon.com", tToken);
                } catch (Exception aCause) {
                    Log.e(MainFragment.class.getSimpleName(), "", aCause);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String aIdentityId) {
                EventBus.getDefault().post(new MainActivity.UpdateProgressRequestEvent().withVisible(false));
                if (aIdentityId == null) {
                    MessageDialog.newInstance("IDを取得できません", "再度お試し下さい", MessageDialog.Button.buildClose(), REQ_ERROR_DIALOG, true, MainFragment.this).show(getActivity().getSupportFragmentManager());
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().post(new MainActivity.UpdateProgressRequestEvent().withVisible(false));
        requestRefreshItems();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    private void generateIdentityRequestIfNeeded() {
        if(mApplication.getApplicationAccountManager().getCachedId() != null) {
            return;
        }

        mApplication.execute(new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                EventBus.getDefault().post(new MainActivity.UpdateProgressRequestEvent().withVisible(true));
            }

            @Override
            protected String doInBackground(Void... params) {
                return mApplication.getApplicationAccountManager().getIdentityId();
            }

            @Override
            protected void onPostExecute(String aIdentityId) {
                EventBus.getDefault().post(new MainActivity.UpdateProgressRequestEvent().withVisible(false));
                if(aIdentityId != null) {
                    requestRefreshItems();
                } else {
                    MessageDialog.newInstance("IDを取得できません", "再度お試し下さい", MessageDialog.Button.buildClose(), REQ_ERROR_DIALOG, true, MainFragment.this).show(getActivity().getSupportFragmentManager());
                }
            }
        });
    }

    private void requestRefreshItems() {
        if(mApplication.getApplicationAccountManager().getCachedId() != null)
            S3ListObjectsService.startService(getActivity().getApplication());
    }

    private void updateFileListItems(List<String> aList) {
        final ListView tListView = (ListView) mView.findViewById(R.id.listFiles);
        if (tListView.getAdapter() == null) {
            tListView.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, aList));
        } else {
            ArrayAdapter<String> tArrayAdapter = (ArrayAdapter) tListView.getAdapter();
            tArrayAdapter.clear();
            tArrayAdapter.addAll(aList);
        }
    }

    public void onEventMainThread(S3ListObjectsService.Event aEvent) {
        if(aEvent.getKeys() != null) {
            updateFileListItems(aEvent.getKeys());
        }
    }

    public void onEventMainThread(S3DeleteObjectService.Event aEvent) {
        if(aEvent.isDone)
            requestRefreshItems();
    }

    public void onEventMainThread(S3PutObjectService.Event aEvent) {
        if(aEvent.isDone)
            requestRefreshItems();
    }

    public void onEventMainThread(ApplicationAccountManager.IdentityChangedEvent aEvent) {
        ((TextView)mView.findViewById(R.id.txtId)).setText(aEvent.getNewIdentityId() == null ? "" : aEvent.getNewIdentityId());
        requestRefreshItems();
    }
}