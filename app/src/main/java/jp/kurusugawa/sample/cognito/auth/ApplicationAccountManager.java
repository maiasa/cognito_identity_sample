package jp.kurusugawa.sample.cognito.auth;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.auth.IdentityChangedListener;
import com.amazonaws.services.cognitoidentity.AmazonCognitoIdentityClient;

import de.greenrobot.event.EventBus;

public class ApplicationAccountManager {

    private final AmazonCognitoIdentityClient mCognitoClient;
    private final CognitoCachingCredentialsProvider mProvider;

    public static class IdentityChangedEvent {
        private final String mOldIdentityId;
        private final String mNewIdentityId;

        public IdentityChangedEvent(String aOldIdentityId, String aNewIdentityId) {
            mOldIdentityId = aOldIdentityId;
            mNewIdentityId = aNewIdentityId;
        }

        public String getOldIdentityId() {
            return mOldIdentityId;
        }

        public String getNewIdentityId() {
            return mNewIdentityId;
        }
    }

    public ApplicationAccountManager(CognitoCachingCredentialsProvider aProvider) {
        mProvider = aProvider;
        mProvider.registerIdentityChangedListener(new IdentityChangedListener() {
            @Override
            public void identityChanged(String aOldIdentityId, String aNewIdentityId) {
                EventBus.getDefault().post(new IdentityChangedEvent(aOldIdentityId, aNewIdentityId));
            }
        });
        mCognitoClient = new AmazonCognitoIdentityClient(mProvider);
        mCognitoClient.setEndpoint("cognito-identity.ap-northeast-1.amazonaws.com");
    }

    /**
     *この端末に設定されているIdentityIDを取得する
     * @return IDが未発行ならNULL、発行済であればオフライン上に保存されているIDを返す
     */
    public String getCachedId() {
        return mProvider.getCachedIdentityId();
    }

    /**
     * 現在のIDを破棄して新しいIdentityIDを発行する
     * @return 新たに発行したゲストID
     */
    public String getIdentityId() {
        mProvider.clear();
        return mProvider.getIdentityId();
    }

    public String getIdentityId(String aProviderId, String aToken) {
        mProvider.getLogins().put(aProviderId, aToken);
        mProvider.setLogins(mProvider.getLogins());
        return mProvider.getIdentityId();
    }

    public AWSCredentials getCredentials() {
        // トークン有効期限が切れていたりすると、NotAuthorizedExceptionがスローされることがある
        return mProvider.getCredentials();
    }

}
