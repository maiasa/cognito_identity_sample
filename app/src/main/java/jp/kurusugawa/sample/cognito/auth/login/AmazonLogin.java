package jp.kurusugawa.sample.cognito.auth.login;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.amazon.identity.auth.device.AuthError;
import com.amazon.identity.auth.device.authorization.api.AmazonAuthorizationManager;
import com.amazon.identity.auth.device.authorization.api.AuthorizationListener;
import com.amazon.identity.auth.device.authorization.api.AuthzConstants;
import com.amazon.identity.auth.device.shared.APIListener;

import java.util.concurrent.CountDownLatch;

public class AmazonLogin {

    private final AmazonAuthorizationManager mAmazonAuthorizationManager;
    private static final String[] AMAZON_APP_SCOPES= {"profile"};

    public AmazonLogin(Context aContext) {
        try {
            mAmazonAuthorizationManager = new AmazonAuthorizationManager(aContext, Bundle.EMPTY);
        } catch(IllegalArgumentException e) {
            Log.e(AmazonAuthorizationManager.class.getSimpleName(), "Unable to Use Amazon Authorization Manager. APIKey is incorrect or does not exist.", e);
            throw e;
        }
    }

    public void login() throws Exception {
        // このメソッドの呼び出し側のネストが深くなりコードが読みづらくなることを避けるため、非同期処理を同期処理に変えた。
        // AmazonAuthorizationManager#authorizeメソッドは戻り値としてFutureを返すが、実際の動きをみると、常にnullが返ってくるので(バグ？)、
        // Futureは使わず、CountDownLatchを使って同期処理にした。

        final CountDownLatch tLatch = new CountDownLatch(1);
        final ThrowableValue<Void> tResult = new ThrowableValue<Void>();

        mAmazonAuthorizationManager.authorize(AMAZON_APP_SCOPES, Bundle.EMPTY, new AuthorizationListener() {
            @Override
            public void onSuccess(Bundle aBundle) {
                tLatch.countDown();
            }

            @Override
            public void onError(AuthError aAuthError) {
                tResult.setThrowable(aAuthError);
                tLatch.countDown();
            }

            @Override
            public void onCancel(Bundle aBundle) {
                Log.e(AmazonLogin.class.getSimpleName(), "onCanceled");
                tLatch.countDown();
            }
        });

        tLatch.await();
        tResult.tryThrow();
    }

    public String getToken() throws Exception {
        // loginメソッドと同様
        // AmazonAuthorizationManager#getTokenメソッドはAmazonAuthorizationManager#authorizeメソッドと違い、nullでない値を返すため、Futureを使った実装も可能だが、loginメソッドと構造をあわせておく。

        final CountDownLatch tLatch = new CountDownLatch(1);
        final ThrowableValue<String> tResult = new ThrowableValue<String>();

        mAmazonAuthorizationManager.getToken(AMAZON_APP_SCOPES, new APIListener() {
            @Override
            public void onSuccess(Bundle aBundle) {
                tResult.setValue(aBundle.getString(AuthzConstants.BUNDLE_KEY.TOKEN.val));
                tLatch.countDown();
            }

            @Override
            public void onError(AuthError aAuthError) {
                tResult.setThrowable(aAuthError);
                tLatch.countDown();
            }
        });

        tLatch.await();
        return tResult.get();
    }
}

class ThrowableValue<_Value> {
    private _Value mValue;
    private Exception mThrowable;

    public void setValue(_Value aValue) {
        mValue = aValue;
    }

    public void setThrowable(Exception aThrowable) {
        mThrowable = aThrowable;
    }

    public _Value get() throws Exception {
        tryThrow();

        return mValue;
    }

    public void tryThrow() throws Exception {
        if(mThrowable != null)
            throw mThrowable;
    }
}