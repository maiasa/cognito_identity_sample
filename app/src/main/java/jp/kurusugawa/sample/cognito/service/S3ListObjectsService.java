package jp.kurusugawa.sample.cognito.service;

import android.app.Application;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import jp.kurusugawa.sample.cognito.CognitoSampleApplication;
import jp.kurusugawa.sample.cognito.R;
import jp.kurusugawa.sample.cognito.util.Util;

public class S3ListObjectsService extends IntentService {

    private CognitoSampleApplication mApplication;
    private AmazonS3Client mAmazonS3Client;
    private String mBucketName;

    public static class Event {
        private List<String> keys;

        public Event(List<String> aKeys) {
            keys = aKeys;
        }

        public List<String> getKeys() {
            return keys;
        }
    }

    public S3ListObjectsService() {
        super(S3ListObjectsService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = (CognitoSampleApplication)getApplication();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            mAmazonS3Client = new AmazonS3Client(mApplication.getApplicationAccountManager().getCredentials());
            mAmazonS3Client.setEndpoint("s3-ap-northeast-1.amazonaws.com");
            mAmazonS3Client.setS3ClientOptions(new S3ClientOptions().withPathStyleAccess(true));
            mBucketName = getString(R.string.bucket_name);
            EventBus.getDefault().post(new Event(getObjectList()));
        } catch (Throwable aThrowable) {
            Log.e(getClass().getSimpleName(), aThrowable.getMessage(), aThrowable);
            EventBus.getDefault().post(new Event(new ArrayList<String>()));
        }
    }

    private List<String> getObjectList() {
        List<String> tResult = new ArrayList<>();
        final String tBaseKey = Util.getRemoteBaseKey(mApplication.getApplicationAccountManager().getCachedId());
        final int tRemoteBasePathLength = tBaseKey.length();
        ObjectListing tObjectListing = mAmazonS3Client.listObjects(mBucketName, tBaseKey);
        do {
            List<S3ObjectSummary> tSummaries = tObjectListing.getObjectSummaries();
            for (S3ObjectSummary tSummary : tSummaries) {
                final String tFileName = tSummary.getKey().substring(tRemoteBasePathLength);
                tResult.add(tFileName);
            }
            tObjectListing = mAmazonS3Client.listNextBatchOfObjects(tObjectListing);
        } while (tObjectListing.getMarker() != null);

        return tResult;
    }

    public static void startService(Application aApplication) {
        Intent tIntent = new Intent(aApplication, S3ListObjectsService.class);
        aApplication.startService(tIntent);
    }
}
