package jp.kurusugawa.sample.cognito;

import android.app.Application;
import android.os.AsyncTask;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jp.kurusugawa.sample.cognito.auth.ApplicationAccountManager;

public class CognitoSampleApplication extends Application {

    private ApplicationAccountManager mApplicationAccountManager;
    private ExecutorService mExecutorService;

    @Override
    public void onCreate() {
        super.onCreate();

        mExecutorService = Executors.newFixedThreadPool(8);

        final String tAwsAccountId = getString(R.string.aws_account_id);
        final String tUnAuthRole   = getString(R.string.unauth_role);
        final String tAuthRole     = getString(R.string.auth_role);

        if(tAwsAccountId.isEmpty() && tUnAuthRole.isEmpty() && tAuthRole.isEmpty()) {
            mApplicationAccountManager = new ApplicationAccountManager(new CognitoCachingCredentialsProvider(
                    getApplicationContext(),
                    getString(R.string.amazon_cognito_identity_pool_id),
                    Regions.AP_NORTHEAST_1
            ));
        } else {
            mApplicationAccountManager = new ApplicationAccountManager(new CognitoCachingCredentialsProvider(
                    getApplicationContext(),
                    tAwsAccountId,
                    getString(R.string.amazon_cognito_identity_pool_id),
                    tUnAuthRole,
                    tAuthRole,
                    Regions.AP_NORTHEAST_1
            ));
        }
    }

    public ApplicationAccountManager getApplicationAccountManager() {
        return mApplicationAccountManager;
    }

    public File getRootDir() {
        return getApplicationContext().getFilesDir().getAbsoluteFile();
    }

    public <_Param, _Progress, _Result> void execute(AsyncTask<_Param, _Progress, _Result> aAsyncTask, _Param... aParams) {
        aAsyncTask.executeOnExecutor(mExecutorService, aParams);
    }
}
