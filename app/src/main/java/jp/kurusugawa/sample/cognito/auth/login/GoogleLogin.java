package jp.kurusugawa.sample.cognito.auth.login;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;

import java.io.IOException;

import jp.kurusugawa.sample.cognito.R;

public class GoogleLogin {
    private final Context mContext;

    public GoogleLogin(Context aContext) {
        mContext = aContext;
    }

    public String getToken() {
        final Account[] tAccounts = AccountManager.get(mContext).getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        if (tAccounts.length <= 0) {
            throw new RuntimeException("google account not found.");
        }

        return getToken(tAccounts[0]);
    }

    private String getToken(final Account aAccount) {
        final String tClientId = mContext.getString(R.string.google_auth_client_id);
        try {
            return GoogleAuthUtil.getToken(mContext, aAccount, "audience:server:client_id:" + tClientId);
        } catch (IOException | GoogleAuthException e) {
            throw new RuntimeException(e);
        }
    }
}