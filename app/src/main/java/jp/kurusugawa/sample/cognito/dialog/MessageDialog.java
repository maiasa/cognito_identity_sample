package jp.kurusugawa.sample.cognito.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import java.io.Serializable;

public class MessageDialog extends DialogFragment {

    private static final String ARG_MESSAGE = "ARG_MESSAGE";
    private static final String ARG_TITLE = "ARG_TITLE";
    private static final String ARG_CANCELABLE = "ARG_CANCELABLE";
    private static final String ARG_BUTTONS = "ARG_BUTTONS";
    private static final String ARG_REQUEST_CODE = "ARG_REQUEST_CODE";
    private static final String ARG_BUNDLE = "ARG_BUNDLE";

    private static final String DEFAULT_TAG = MessageDialog.class.getSimpleName();

    public static class Button implements Serializable {

        public static final int POSITIVE = 0x01;
        public static final int NEGATIVE = 0x02;
        public static final int CLOSE    = 0x04;

        private final int mId;
        private final String mCaption;

        public Button(int aId, String aCaption) {
            mId = aId;
            mCaption = aCaption;
        }

        public int getId() {
            return mId;
        }

        public String getCaption() {
            return mCaption;
        }

        public static Button[] buildYesNo() {
            return new Button[]{ new Button(POSITIVE, "はい"), new Button(NEGATIVE, "いいえ") };
        }

        public static Button[] buildClose() {
            return new Button[]{ new Button(CLOSE, "閉じる") };
        }
    }

    public static MessageDialog newInstance(String aTitle, String aMessage, Button[] aButtons, int aRequestCode, boolean aCancelable, Fragment aFragment) {
        return newInstance(aTitle, aMessage, aButtons, aRequestCode, new Bundle(), aCancelable, aFragment);
    }

    public static MessageDialog newInstance(String aTitle, String aMessage, Button[] aButtons, int aRequestCode, boolean aCancelable) {
        return newInstance(aTitle, aMessage, aButtons, aRequestCode, new Bundle(), aCancelable, null);
    }

    public static MessageDialog newInstance( String aTitle, String aMessage, Button[] aButtons, final int aRequestCode, Bundle aBundle, boolean aCancelable) {
        return newInstance(aTitle, aMessage, aButtons, aRequestCode, aBundle, aCancelable, null);
    }

    public static MessageDialog newInstance(String aTitle, String aMessage, Button[] aButtons, final int aRequestCode, Bundle aBundle, boolean aCancelable, final Fragment aFragment) {
        final MessageDialog tMessageDialog = new MessageDialog();
        final Bundle tBundle = new Bundle();
        tBundle.putString(ARG_TITLE, aTitle);
        tBundle.putString(ARG_MESSAGE, aMessage);
        tBundle.putBoolean(ARG_CANCELABLE, aCancelable);
        tBundle.putInt(ARG_REQUEST_CODE, aRequestCode);
        tBundle.putSerializable(ARG_BUTTONS, aButtons);
        tBundle.putBundle(ARG_BUNDLE, aBundle);
        tMessageDialog.setArguments(tBundle);
        if(aFragment != null) {
            tMessageDialog.setTargetFragment(aFragment, aRequestCode);
        }
        return tMessageDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Serializable tSerializable = getArguments().getSerializable(ARG_BUTTONS);
        if(!(tSerializable instanceof Button[])) {
            throw new IllegalArgumentException();
        }

        final Button[] tButtons = (Button[]) tSerializable;
        final int tButtonLength = tButtons.length;
        if(tButtonLength > 2 || tButtonLength <= 0) {
            throw new IllegalArgumentException();
        }

        final AlertDialog.Builder tBuilder = new AlertDialog.Builder(getActivity())
                .setTitle(getArguments().getString(ARG_TITLE))
                .setMessage(getArguments().getString(ARG_MESSAGE))
                .setCancelable(getArguments().getBoolean(ARG_CANCELABLE));

        switch (tButtonLength) {
            case 2:
                tBuilder.setNegativeButton(tButtons[1].getCaption(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface aDialog, int aWhich) {
                        finish(tButtons[1]);
                        aDialog.dismiss();
                    }
                });

            case 1:
                tBuilder.setPositiveButton(tButtons[0].getCaption(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface aDialog, int aWhich) {
                        finish(tButtons[0]);
                        aDialog.dismiss();
                    }
                });
                break;

            default:
                throw new IllegalArgumentException();
        }

        return tBuilder.create();
    }

    private void finish(Button aButton) {
        if(getTargetFragment() != null) {
            getTargetFragment().onActivityResult(getArguments().getInt(ARG_REQUEST_CODE),aButton.getId(), new Intent().putExtras(getArguments().getBundle(ARG_BUNDLE)));
        }
    }

    public void show(FragmentManager aFragmentManager) {
        //XXX ※このやり方は強引な方法（トランザクションへの変更が失われる可能性がある）なので注意して下さい。
        //    画面を失った状態でも安全にFragmentを操作したい場合は真面目にキュー管理を実装する必要があります。
        aFragmentManager.beginTransaction().add(this, DEFAULT_TAG).commitAllowingStateLoss();
    }
}
