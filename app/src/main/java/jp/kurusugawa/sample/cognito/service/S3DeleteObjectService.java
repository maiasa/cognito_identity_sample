package jp.kurusugawa.sample.cognito.service;

import android.app.Application;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;

import java.io.File;
import java.io.Serializable;

import de.greenrobot.event.EventBus;
import jp.kurusugawa.sample.cognito.CognitoSampleApplication;
import jp.kurusugawa.sample.cognito.R;
import jp.kurusugawa.sample.cognito.util.Util;

public class S3DeleteObjectService extends IntentService {

    private static final String ARG_FILE = "ARG_FILE";
    private CognitoSampleApplication mApplication;
    private AmazonS3Client mAmazonS3Client;
    private String mBucketName;

    public static class Event {
        public boolean isDone;
    }

    public static void startService(Application aApplication, File aFile) {
        Intent tIntent = new Intent(aApplication, S3DeleteObjectService.class);
        tIntent.putExtra(ARG_FILE, aFile);
        aApplication.startService(tIntent);
    }

    public S3DeleteObjectService() {
        super(S3DeleteObjectService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = (CognitoSampleApplication)getApplication();
    }

    @Override
    protected void onHandleIntent(Intent aIntent) {
        final Event tEvent = new Event();
        try {
            final Serializable tSerializableExtra = aIntent.getSerializableExtra(ARG_FILE);
            if (!(tSerializableExtra instanceof File))
                throw new IllegalArgumentException();

            final File tFile = (File) tSerializableExtra;

            mAmazonS3Client = new AmazonS3Client(mApplication.getApplicationAccountManager().getCredentials());
            mAmazonS3Client.setEndpoint("s3-ap-northeast-1.amazonaws.com");
            mAmazonS3Client.setS3ClientOptions(new S3ClientOptions().withPathStyleAccess(true));
            mBucketName = getString(R.string.bucket_name);

            final int tBasePathLength = mApplication.getRootDir().getAbsolutePath().length();
            deleteFile(tFile, tBasePathLength);

            tEvent.isDone = true;
        } catch (Throwable aThrowable) {
            Log.e(getClass().getSimpleName(), aThrowable.getMessage(), aThrowable);
            tEvent.isDone = false;
        }
        EventBus.getDefault().post(tEvent);
    }

    private void deleteFile(File aFile, int aBasePathLength) {
        final String tKey = aFile.getAbsolutePath().substring(aBasePathLength + 1);
        final String tFullKey = String.format("%s%s", Util.getRemoteBaseKey(mApplication.getApplicationAccountManager().getCachedId()), tKey);
        mAmazonS3Client.deleteObject(mBucketName, tFullKey);
    }
}
