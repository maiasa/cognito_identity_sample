package jp.kurusugawa.sample.cognito.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

public class Util {

    public static void closeQuietly(Closeable... aCloseables) {
        if(aCloseables == null || aCloseables.length == 0) {
            return;
        }

        for(Closeable tCloseable : aCloseables) {
            if(tCloseable == null) {
                continue;
            }

            try {
                tCloseable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static byte[] toGzip(File aFile) throws IOException {
        ByteArrayOutputStream tByteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream tGzipOutputStream = new GZIPOutputStream(tByteArrayOutputStream);

        FileInputStream tFileInputStream = new FileInputStream(aFile);
        BufferedInputStream tBufferedInputStream = new BufferedInputStream(tFileInputStream);

        byte[] tBuffer = new byte[4096];
        int tSize;
        try {
            while ((tSize = tBufferedInputStream.read(tBuffer)) >= 0) {
                tGzipOutputStream.write(tBuffer, 0, tSize);
            }
        } finally {
            closeQuietly(tBufferedInputStream, tGzipOutputStream);
        }

        return tByteArrayOutputStream.toByteArray();
    }

    public static String getRemoteBaseKey(String aId) {
        return String.format("cognito/sample/%s/", aId);
    }
}
