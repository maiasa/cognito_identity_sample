package jp.kurusugawa.sample.cognito.fragment;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jp.kurusugawa.sample.cognito.CognitoSampleApplication;
import jp.kurusugawa.sample.cognito.R;
import jp.kurusugawa.sample.cognito.service.S3PutObjectService;

public class SensorLoggingFragment extends Fragment {

    private View mView;
    private SensorManager mSensorManager;
    private SensorEventListener mSensorEventListener;
    private File mLogFile;
    private FourAxisLogger mFourAxisLogger;

    private static final String STATE_LOG_FILE = "STATE_LOG_FILE";

    public static final int RESPONSE_CODE_FINISH = 0x01;

    public static SensorLoggingFragment newInstance(int aRequestCode, Fragment aFragment) {
        final SensorLoggingFragment tFragment = new SensorLoggingFragment();
        tFragment.setTargetFragment(aFragment, aRequestCode);
        return tFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_logging, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            mLogFile = savedInstanceState == null ?
                    new File(getAccelerationDir()
                            , String.format("%s.tsv", new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date())))
                    : (File)savedInstanceState.getSerializable(STATE_LOG_FILE);

            assert mLogFile != null;
            if(!mLogFile.exists() && !mLogFile.createNewFile())
                throw new IOException("can't create new file.");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        initializeView();
    }

    private File getAccelerationDir() {
        return getDir(new File(((CognitoSampleApplication)getActivity().getApplication()).getRootDir(), "acc/"));
    }

    private File getDir(File aDir) {
        if(!aDir.exists() && !aDir.mkdirs())
            throw new RuntimeException("can't create dirs.");
        return aDir;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_LOG_FILE, mLogFile);
    }

    private void initializeView() {
        mView.findViewById(R.id.btnStop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(SensorLoggingFragment.this).commit();
                getTargetFragment().onActivityResult(getTargetRequestCode(), RESPONSE_CODE_FINISH, null);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        beginLogging();
    }

    @Override
    public void onStop() {
        super.onStop();
        finishLogging();
    }

    private void beginLogging() {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        try {
            mFourAxisLogger = new FourAxisLogger(mLogFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        mSensorManager = (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);

        final List<Sensor> tSensorList = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
        if(tSensorList.size() <= 0) {
            Log.e(SensorLoggingFragment.class.getSimpleName(), "accelerometer is not found.");
            return;
        }
        final Sensor tTargetSensor = tSensorList.get(0);

        mSensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                try {
                    mFourAxisLogger.append(event.timestamp, event.values[0], event.values[1], event.values[2]);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        mSensorManager.registerListener(mSensorEventListener, tTargetSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    private void finishLogging() {
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(mSensorEventListener != null)
            mSensorManager.unregisterListener(mSensorEventListener);
        try {
            mFourAxisLogger.close();

            S3PutObjectService.startService(getActivity().getApplication(), mLogFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

class FourAxisLogger {
    private final Writer mWriter;
    private final StringBuilder mRecordBuilder = new StringBuilder();
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public FourAxisLogger(File aOutFile) throws FileNotFoundException {
        mWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(aOutFile), Charset.forName("UTF-8")), 1024 * 128);
    }

    public void append(long aTimestamp, float aX, float aY, float aZ) throws IOException {
        mRecordBuilder.setLength(0);
        mRecordBuilder.append(String.valueOf(aTimestamp)).append('\t').append(String.valueOf(aX)).append('\t').append(String.valueOf(aY)).append('\t').append(String.valueOf(aZ)).append(LINE_SEPARATOR);
        mWriter.write(mRecordBuilder.toString());
    }

    public void close() throws IOException {
        mWriter.close();
    }
}