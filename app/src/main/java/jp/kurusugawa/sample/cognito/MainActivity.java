package jp.kurusugawa.sample.cognito;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import de.greenrobot.event.EventBus;
import jp.kurusugawa.sample.cognito.fragment.MainFragment;

public class MainActivity extends FragmentActivity {

    public static class UpdateProgressRequestEvent {
        private boolean mIsVisible;
        public UpdateProgressRequestEvent withVisible(boolean aIsVisible) {
            mIsVisible = aIsVisible;
            return this;
        }
        public boolean isVisible() {
            return mIsVisible;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.root, MainFragment.newInstance()).commit();
        }

        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(UpdateProgressRequestEvent aEvent) {
        findViewById(R.id.progressBar).setVisibility(aEvent.isVisible() ? View.VISIBLE : View.GONE);
    }
}
